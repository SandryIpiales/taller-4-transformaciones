
package org.seipiales;


import javax.media.opengl.GL;


public class Dibujar_Rey {
    //a=lado izquierdo b=lado arriba c=lado derecho d=lado abajo
    public static double a1 = -1.0,b1 = -1.0,c1 = 1.0, d1 =-3.0;
    
    public static void DibujarRey(GL gl){
        
        //Torso
          Cuadrado.x=a1+0.4;
          Cuadrado.y=b1+5.8;
          Cuadrado.z=c1-0.4;
          Cuadrado.w=d1+5.8;
          Cuadrado.dibujarCuadrado(gl,0.7f, 0.4498f, 0.3723f);
        //Brazo  Izquierdo
          Cuadrado.x=a1+0.1;
          Cuadrado.y=b1+5.8;
          Cuadrado.z=c1-1.65;
          Cuadrado.w=d1+6.8;
          Cuadrado.dibujarCuadrado(gl,0.7f, 0.4498f, 0.3723f);
          
         //Brazo Derecho
          Cuadrado.x=a1+1.65;
          Cuadrado.y=b1+5.8;
          Cuadrado.z=c1-0.1;
          Cuadrado.w=d1+6.8;
          Cuadrado.dibujarCuadrado(gl,0.7f, 0.4498f, 0.3723f);
          
         //Cuello
          Cuadrado.x=a1+0.8;
          Cuadrado.y=b1+6.0;
          Cuadrado.z=c1-0.8;
          Cuadrado.w=d1+7.8;
          Cuadrado.dibujarCuadrado(gl,1.0f, 0.9493f, 0.81f);
          
        //Cabeza
        gl.glPushMatrix();
        gl.glScalef(0.5f, 0.5f, 0.5f);
        gl.glTranslatef(0f, 10.7f, 0);
        Circulo.dibujarCirculo(gl,1.0f, 0.9493f, 0.81f);
        gl.glPopMatrix();
       
          
    }
 
    public static void DibujarCorona (GL gl){

          Cuadrado.x=a1+0.5;
          Cuadrado.y=b1+6.8;
          Cuadrado.z=c1-0.5;
          Cuadrado.w=d1+8.45;
          Cuadrado.dibujarCuadrado(gl,0.92f, 0.6012f, 0.0092f);
          
          gl.glPushMatrix();
          gl.glScalef(0.2f, 0.2f, 0.2f);
          gl.glTranslatef(-1.5f,28.9f, 0);
          Triangulo.x1=0.0;
          Triangulo.y1=2.0;
          Triangulo.x2=-1.0;
          Triangulo.y2=0.0;
          Triangulo.x3=1.0;
          Triangulo.y3=0.0;
          Triangulo.dibujarTriangulo(gl, 0.92f, 0.6012f, 0.0092f);
          gl.glPopMatrix();
          
          gl.glPushMatrix();
          gl.glScalef(0.2f, 0.2f, 0.2f);
          gl.glTranslatef(0.0f,28.9f, 0);
          Triangulo.x1=0.0;
          Triangulo.y1=2.1;
          Triangulo.x2=-1.0;
          Triangulo.y2=0.0;
          Triangulo.x3=1.0;
          Triangulo.y3=0.0;
          Triangulo.dibujarTriangulo(gl, 0.92f, 0.6012f, 0.0092f);
          gl.glPopMatrix();
          
          gl.glPushMatrix();
          gl.glScalef(0.2f, 0.2f, 0.2f);
          gl.glTranslatef(1.5f,28.9f, 0);
          Triangulo.x1=0.0;
          Triangulo.y1=2.0;
          Triangulo.x2=-1.0;
          Triangulo.y2=0.0;
          Triangulo.x3=1.0;
          Triangulo.y3=0.0;
          Triangulo.dibujarTriangulo(gl, 0.92f, 0.6012f, 0.0092f);
          gl.glPopMatrix();
          
    }
    
}
