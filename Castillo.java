package org.seipiales;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
//Libreria para frame
import javax.swing.JFrame;
//librerias para teclas
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import com.sun.opengl.util.Animator;


/**
* Castillo.java 
 * Autoras: Guaminga A., G�mez V., Ipiales S.
 * Este programa contiene el escenario 2
 * 1.	Castillo con banderas rotando
 * 2.	Un sol girando
 * 3.	Rey caminando
 * 2020-08-06
 */

public class Castillo extends JFrame implements KeyListener{
    //Variables para instanciar funciones de opengl
        static GL gl;
        static GLU glu;
        Teclado t= new Teclado();

    public Castillo() {
        setSize(840, 680);
        setLocation(10, 40);
        setTitle("Castillo");
        GraphicListener listener = new GraphicListener();
        GLCanvas canvas = new GLCanvas(new GLCapabilities());
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);        
        Animator animator = new Animator(canvas);
        animator.start();        
        addKeyListener(this);
    }

    public static void main(String args[]) {
        Castillo myframe = new Castillo();
        myframe.setVisible(true);
        myframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
   
    float angulo=0, trasladaX1=0;
//    float trasladaA=0;
    public class GraphicListener implements GLEventListener {

        public void display(GLAutoDrawable arg0) {
            //Inicializamos las variables
            //INICIALIZA GLU
            glu = new GLU();
            //Inicializa Gl
            gl = arg0.getGL();

            gl.glClear(gl.GL_COLOR_BUFFER_BIT);
            //Establecemos el color de fondo de la ventana de colores RGB
            gl.glClearColor(0.4455f, 0.81f, 0.7614f,0f);            
          gl.glMatrixMode(GL.GL_PROJECTION);
            //Matriz identidad
            gl.glLoadIdentity();
            
           Castillo_Dibujo castillo = new Castillo_Dibujo();
           Dibujar_Rey rey= new Dibujar_Rey();
           Figuras fig= new Figuras();
           gl.glPopMatrix();
           gl.glTranslatef(0, -0.2f, 0);
           gl.glScalef(0.08f, 0.1f, 0);
           fig.DibujarCuadrado2Color(gl, 0, 0, 0, 0, 0, 0, -15f, 15f,-5f ,-11f);
           gl.glPopMatrix();
                   
            
           
           castillo.DibujarCastillo(gl);
           
           gl.glPushMatrix();
           gl.glTranslated(t.trasladaX, 0f, 0f);
           Dibujar_Rey.DibujarRey(gl);
           gl.glPopMatrix();
           
           gl.glPushMatrix();
           gl.glTranslated(t.trasladaX, 0f, 0);
           Dibujar_Rey.DibujarCorona(gl);
           gl.glPopMatrix();
           
           gl.glPushMatrix();
            Nube n= new Nube();
            gl.glTranslatef(-2f, -5f, 0);
            n.DibujarNube(gl);
            gl.glPopMatrix();
           gl.glPushMatrix();
           castillo.DibujarTecho(gl);
           gl.glPopMatrix();
           
            gl.glPushMatrix();
            castillo.DibujarBandera(gl);
            gl.glTranslated(-4f, 9f, 0f);
            gl.glTranslatef(trasladaX1, 0, 0);
            gl.glRotatef(angulo, 0, 0, 1f);
            Circulo.dibujarCirculo(gl, 0.92f, 0.6012f, 0.0092f);
            gl.glTranslatef(0.58f, 1.19f, 0);
            gl.glScalef(0.59f, 0.59f, 0);
            Orbitas orbita= new Orbitas( gl,1f, 1f, 2f, 2, 0.92f, 0.6012f, 0.0092f);
            orbita.dibujarOrbita();
            gl.glPopMatrix();
            
            
            gl.glFlush();
            angulo+=0.2;
                       
            if (trasladaX1<=16f) {
               trasladaX1+=0.05; 
            }else{
                trasladaX1+=-21;
            }
            
            
            System.out.println(trasladaX1);
        }

        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        }

        //En este metodo se queda el mismo segmento que hemos venido manejando
        public void init(GLAutoDrawable arg0) {
            GL gl = arg0.getGL();
            gl.glEnable(GL.GL_BLEND);
            gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        }

        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        }
    }

    //Metodo para el teclado
    public void keyPressed(KeyEvent e) {
        t.keyPressed(e);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}

