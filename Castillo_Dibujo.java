
package org.seipiales;

import javax.media.opengl.GL;

public class Castillo_Dibujo extends Cuadrado {
    
    Cuadrado cuadrado = new Cuadrado();
    static float angulo=0;
    //a=lado izquierdo b=lado arriba c=lado derecho d=lado abajo
    double a = -1.0,b = -1.0,c = 1.0, d =-3.0;
    public void DibujarCastillo(GL gl){
        
            //Pared intermedia
          cuadrado.a=a-2.6;
          cuadrado.b=b;
          cuadrado.c=c+2.6;
          cuadrado.d=d-3;
          cuadrado.dibujar(gl, 0.69f, 0.5244f, 0.3588f);
          

//        Puerta - Pared intermedia  
          cuadrado.a=a-1.2;
          cuadrado.b=b-1;
          cuadrado.c=c+1.2;
          cuadrado.d=d-3;
          cuadrado.dibujar(gl, 0.21f, 0.1398f, 0.0861f);
     
//        Torre Superior
          cuadrado.a=a-1.4;
          cuadrado.b=b+3.8;
          cuadrado.c=c+1.4;
          cuadrado.d=d+2.2;
          cuadrado.dibujar(gl, 0.69f, 0.5244f, 0.3588f);

      //Torre Superior-Ventana
          cuadrado.a=a+0.4;
          cuadrado.b=b+3.6;
          cuadrado.c=c-0.4;
          cuadrado.d=d+4.3;
          cuadrado.dibujar(gl, 0.21f, 0.1398f, 0.0861f);
         
//    //Torre Izquierda
          cuadrado.a=a-5.6;
          cuadrado.b=b+1.5;
          cuadrado.c=c-4.7;
          cuadrado.d=d-3;
          cuadrado.dibujar(gl,0.69f, 0.5244f, 0.3588f);

//    //Ventana Torre Izquierda
          cuadrado.a=a-4.7;
          cuadrado.b=b+0.5;
          cuadrado.c=c-5.6;
          cuadrado.d=d+1.0;
          cuadrado.dibujar(gl, 0.21f, 0.1398f, 0.0861f);

//     //Torre Derecha
          cuadrado.a=a+4.7;
          cuadrado.b=b+1.5;
          cuadrado.c=c+5.6;
          cuadrado.d=d-3;
          cuadrado.dibujar(gl, 0.69f, 0.5244f, 0.3588f);
         
//      //Ventana Torre Derecha
          cuadrado.a=a+5.6;
          cuadrado.b=b+0.5;
          cuadrado.c=c+4.7;
          cuadrado.d=d+1.0;
          cuadrado.dibujar(gl,0.21f, 0.1398f, 0.0861f);
         

}
    
    public void DibujarTecho(GL gl){
        
        //Techo Torre Derecha
          gl.glPushMatrix();
          gl.glTranslated(6.8f, 0.8f, 0.0f);
          gl.glScalef(0.4f, 0.4f, 0.4f);
          cuadrado.a=a-0.2;
          cuadrado.b=b+2.0;
          cuadrado.c=c;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl, 1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-2.7;
          cuadrado.b=b+2.0;
          cuadrado.c=c-2.5;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-5.2;
          cuadrado.b=b+2.0;
          cuadrado.c=c-5.0;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-7.7;
          cuadrado.b=b+2.0;
          cuadrado.c=c-7.5;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          gl.glPopMatrix();
          
          //Techo Torre Izquierda
          gl.glPushMatrix();
          gl.glTranslated(-3.6f, 0.8f, 0.0f);
          gl.glScalef(0.4f, 0.4f, 0.4f);
          cuadrado.a=a-0.2;
          cuadrado.b=b+2.0;
          cuadrado.c=c;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-2.7;
          cuadrado.b=b+2.0;
          cuadrado.c=c-2.5;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-5.2;
          cuadrado.b=b+2.0;
          cuadrado.c=c-5.0;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-7.7;
          cuadrado.b=b+2.0;
          cuadrado.c=c-7.5;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          gl.glPopMatrix();
          
         //Techo Pared Intermedia
          gl.glPushMatrix();
          gl.glTranslated(2.7f, -0.6f, 0.0f);
          gl.glScalef(0.4f, 0.4f, 0.4f);
          cuadrado.a=a+2.4;
          cuadrado.b=b+2.0;
          cuadrado.c=c+1.3;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-0.2;
          cuadrado.b=b+2.0;
          cuadrado.c=c;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-2.7;
          cuadrado.b=b+2.0;
          cuadrado.c=c-2.5;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-5.2;
          cuadrado.b=b+2.0;
          cuadrado.c=c-5.0;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-7.7;
          cuadrado.b=b+2.0;
          cuadrado.c=c-7.5;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-10.2;
          cuadrado.b=b+2.0;
          cuadrado.c=c-10.0;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-12.7;
          cuadrado.b=b+2.0;
          cuadrado.c=c-12.5;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-15.0;
          cuadrado.b=b+2.0;
          cuadrado.c=c-15.0;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          gl.glPopMatrix();
          
          //Techo Torre Superior
          gl.glPushMatrix();
          gl.glTranslated(2.6f, 3.3f, 0.0f);
          gl.glScalef(0.4f, 0.4f, 0.4f);
          cuadrado.a=a-0.2;
          cuadrado.b=b+2.0;
          cuadrado.c=c;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-2.7;
          cuadrado.b=b+2.0;
          cuadrado.c=c-2.5;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-5.2;
          cuadrado.b=b+2.0;
          cuadrado.c=c-5.0;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-7.7;
          cuadrado.b=b+2.0;
          cuadrado.c=c-7.5;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-10.2;
          cuadrado.b=b+2.0;
          cuadrado.c=c-10.0;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          cuadrado.a=a-12.7;
          cuadrado.b=b+2.0;
          cuadrado.c=c-12.5;
          cuadrado.d=d+1.4;
          cuadrado.dibujar(gl,1.0f, 0.9493f, 0.81f);
          gl.glPopMatrix();
          
          
    }
 
    public void DibujarBandera(GL gl){
         
          //Bandera Derecha
         gl.glBegin(GL.GL_LINES);
         gl.glColor3f(0.21f, 0.1398f, 0.0861f); 
         gl.glVertex2d(a+6.25,0.5);
         gl.glVertex2d(a+6.25,2.5);
         gl.glEnd();
         
         //Triangulo Derecho           
           gl.glPushMatrix();
           gl.glTranslatef(-0.1f, 0.8f, -4f);
            gl.glRotatef(angulo, 0, -1, 1);
            gl.glTranslatef(-0.1f, 0f, 0.1f);
            gl.glRotatef(angulo, 0,1f, -1);
            gl.glBegin(GL.GL_TRIANGLES);
            gl.glColor3f(0.92f, 0.6012f, 0.0092f);
            gl.glVertex3d(a+6.25, 2.5, 0.0f);   // Top
            gl.glVertex3d(a+6.25, 1.5, 0.0f); // Bottom Left
            gl.glVertex3d(a+7.5, 2.0, 0.0f);  // Bottom Right
        gl.glEnd();
         gl.glPopMatrix();
         //Bandera Izquierda
         gl.glBegin(GL.GL_LINES);
         gl.glColor3f(0.21f, 0.1398f, 0.0861f); 
         gl.glVertex2d(c-6.15,0.5);
         gl.glVertex2d(c-6.15,2.5);
         gl.glEnd();
         
//        Triangulo Izquierdo
            gl.glTranslatef(-0.1f, 0.8f, -4f);
            gl.glRotatef(angulo, 0, -1, 1);
            gl.glTranslatef(-0.1f, 0f, 0.1f);
            gl.glRotatef(angulo, 0,1f, -1);
            gl.glBegin(GL.GL_TRIANGLES);
            gl.glColor3f(0.92f, 0.6012f, 0.0092f);    // Set the current drawing color to red
            gl.glVertex3d(c-6.15, 2.5, 0.0f);   // Top
            gl.glVertex3d(c-6.15, 1.5, 0.0f); // Bottom Left
            gl.glVertex3d(c-5.0, 2.0, 0.0f);  // Bottom Right
        gl.glEnd();
    angulo+=1f;
}

    }
    
    
//        