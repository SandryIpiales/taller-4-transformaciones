/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import static java.lang.Math.sin;
import static java.lang.StrictMath.cos;
import javax.media.opengl.GL;

/**
 *
 * @author lenovo
 */
public class Figuras {
    public static float c1, c2, c3, c4, c5, c6, c7, c8,c9, c10, c11, c12, a, b, c, d, e, f,g, h, i,j,x,y;
    
        public Figuras(){}

        public void DibujarCuadrado2Color(GL gl,float c1, float c2, float c3, float c4, float c5, float c6, float a, float b, float c, float d){
            gl.glBegin(GL.GL_QUADS);
            gl.glColor3f(c1, c2, c3); //Color
            gl.glVertex3f(a, c, 0);   //Esquina Superior Izquierda
            gl.glVertex3f(b, c, 0);   //Esquina Superior Derecha
            gl.glColor3f(c4, c5, c6); //Color
            gl.glVertex3f(b, d, 0);   // Esquina Inferior Derecha
            gl.glVertex3f(a, d, 0);   //Esquina Inferior Izquierda

            gl.glEnd();
        }
        public void DibujarTrapecio(GL gl,float c1, float c2, float c3, float c4, float c5, float c6, float a, float b, float ai, float bi,float c, float d){
            gl.glBegin(GL.GL_QUADS);
            gl.glColor3f(c1, c2, c3); //Color
            gl.glVertex3f(a, c, 0);   //Esquina Superior Izquierda
            gl.glVertex3f(b, c, 0);   //Esquina Superior Derecha
            gl.glColor3f(c4, c5, c6); //Color
            gl.glVertex3f(ai, d, 0);   // Esquina Inferior Derecha
            gl.glVertex3f(bi, d, 0);   //Esquina Inferior Izquierda

            gl.glEnd();
        }
        public void DibujarTriangulo(GL gl,float c1, float c2, float c3, float a, float b, float c,float d, float e){
            gl.glBegin(GL.GL_TRIANGLES);
                gl.glColor3f(c1, c2, c3); //Color
                gl.glVertex3f(a, d, 0);   //Vertice Superior
                gl.glVertex3f(b, e, 0);   //Vertice Inferior Izuierdo
                gl.glVertex3f(c, e, 0);   //Vertice Inferior Derecho
            gl.glEnd();
        }
        public void DibujarPentagono(GL gl,float c1, float c2, float c3, float c4, float c5, float c6, float a, float b, float c,float d, float e, float f){
            gl.glBegin(GL.GL_POLYGON);
                gl.glColor3f(c1, c2, c3); //Color
                gl.glVertex3f(a, d, 0);   //Vertice Superior            
                gl.glVertex3f(b, e, 0);   //Vertice Superior Izquierdo
                gl.glVertex3f(b, f, 0);   //Vertice Superior Derecho
                gl.glColor3f(c4, c5, c6); //Color
                gl.glVertex3f(c, f, 0);   //Vertice Inferior Izquierdo
                gl.glVertex3f(c, e, 0);   //Vertice Inferior Derecho
            gl.glEnd();
        }
        
        public void DecagonoMitad(GL gl,float c1, float c2, float c3, float c4, float c5, float c6, float a, float b, float c,float d, float e, float f, float g){
            gl.glBegin(GL.GL_POLYGON);
                gl.glColor3f(c1, c2, c3);   //Color
                gl.glVertex3f(a, d, 0);     //Parte Superior Derecha 
                gl.glVertex3f(b, e, 0);     // Vertice intermedio1 Derecho
                gl.glVertex3f(-b, e, 0);    // Vertice intermedio1 Izquierdo           
                gl.glVertex3f(-a, d, 0);    //Parte Superior Izquierdo
                gl.glColor3f(c4, c5, c6);   //Color
                gl.glVertex3f(-c, f, 0);     // Vertice intermedio2 Derecho
                gl.glVertex3f(-c, g, 0);     //Vertice Inferior Derecho
                
                gl.glVertex3f(c, g, 0);    //// Vertice intermedio2 Izquierdo
                gl.glVertex3f(c, f, 0);    //Vertice Inferior Izquierda
            gl.glEnd();
        }
        public void PoligonoVentanas(GL gl,float c1, float c2, float c3, float c4, float c5, float c6, float a, float b, float c,float d, float e, float f, float g, float h, float i, float j){
            gl.glBegin(GL.GL_POLYGON);
                gl.glColor3f(c1, c2, c3);   //Color
                gl.glVertex3f(a, g, 0);      
                gl.glVertex3f(b, h, 0);     
                gl.glVertex3f(c, i, 0);              
                gl.glVertex3f(c, j, 0);    
                gl.glColor3f(c4, c5, c6);   //Color
                gl.glVertex3f(d, j, 0);     
                gl.glVertex3f(d, i, 0);     
                gl.glVertex3f(e, h, 0);    
                gl.glVertex3f(f, g, 0);    
            gl.glEnd();
        }
        
        public void Circulo(GL gl, float a, float b, float c){
//            float x,y;
            gl.glBegin(GL.GL_POLYGON);
        for (int i = 0; i < 1000; i++) {
            x=(float)(c*Math.cos(i));
            y=(float)(c*Math.sin(i));
            gl.glVertex3f(x+a, y+b, 0f);
            
        }
        gl.glEnd();
        }
        public void Nube(GL gl, float a, float b, float c){
//       float x,y;
        Figuras nube= new Figuras(); 
//        
        gl.glColor3f(1, 1, 1);
        for (int i = 0; i < 19; i++) {
            x= (float) (cos(i+4));
            y= (float) (sin(i+2));
           nube.Circulo(gl, x+a, y+b, c); 
        }
        }
    }
