/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import static java.lang.Math.sin;
import static java.lang.StrictMath.cos;
import javax.media.opengl.GL;

/**
 *
 * @author lenovo
 */
public class Nube {
    
    GL gl;
    
    public Nube(){
    }  
    public void DibujarNube(GL gl){
       
        
        Figuras nube= new Figuras();

        gl.glPushMatrix(); 
        gl.glScalef(1.5f, 1.5f, 0);
        gl.glRotatef(45, 0f, 0, 1f);
        nube.Nube(gl, 2f, 8f, 0.4f);
        gl.glPopMatrix();
        
        gl.glPushMatrix(); 
        gl.glScalef(1.5f, 1.5f, 0);
        gl.glTranslatef(5f, 2, 0);
        gl.glRotatef(45, 0f, 0, 1f);
        nube.Nube(gl, 2f, 8f, 0.4f);
        gl.glPopMatrix();
        
        gl.glPushMatrix(); 
        gl.glScalef(1.5f, 1.5f, 0);
        gl.glTranslatef(10f, 0, 0);
        gl.glRotatef(45, 0f, 0, 1f);
        nube.Nube(gl, 2f, 8f, 0.4f);
        gl.glPopMatrix();
        
        gl.glPushMatrix(); 
        gl.glScalef(1.5f, 1.5f, 0);
        gl.glTranslatef(12f, 2.5f, 0);
        gl.glRotatef(45, 0f, 0, 1f);
        nube.Nube(gl, 2f, 8f, 0.4f);
        gl.glPopMatrix();
 
        }
        
    
    
}
