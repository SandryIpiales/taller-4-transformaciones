/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import javax.media.opengl.GL;


public class Orbitas {
      GL gl;
    float radio;
    float x, y;
    float es;
    float r, g, b;
    
    public Orbitas(GL gl, float radio, float x, float y, float es, float r, float g, float b) {
        this.gl = gl;
        this.radio = radio;
        this.x = x;
        this.y = y;
        this.es = es;
        this.r = r;
        this.g = g;
        this.b = b;
    }
 public void dibujarOrbita() {
        gl.glPushMatrix();
        gl.glColor3f(this.r, this.g, this.b);
        gl.glBegin(gl.GL_LINES);

        for (int i = 0; i < 20; i++) {
            float x = (float) Math.cos(i * 2 * Math.PI / 20);
            float y = (float) Math.sin(i * 2 * Math.PI / 20);
            gl.glVertex2f(this.es * x - this.x, this.es * y - this.y);
        }
        gl.glEnd();
        gl.glPopMatrix();
    }
    
}
